* Histórico, processos e conceitos:
Instalado e implementado a aplicação nodejs no docker, o docker esta utilizando a imagem node:argo (node last stable: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/), depois identificado que a imagem utiliza uma versão de linux customizada e por isso será necessário alterar a imagem base da aplicação, e conforme o estudo está sendo avaliado como será a utilização do docker. 
Analisado o github publico da Chaordic e identifiquei muitos repositorios relacionados ao Ansible, por este motivo estarei utilizando o Ansible para fazer o deploy da aplicação node.
Para a clusterização dos processos em uma instancia foi escolhido a utilização do pm2, que permite fazer o cluster sem qualquer alteração no código, possui parametros de configuração do cluster, monitoramento dos processos da aplicação por cluster e controle de reincialização automatica quando necessario, este módulo do node permite a abstração de muitos componentes e complexidade na tarefa de realizar o cluster do node, monitoramento dos processos, resiliencia dos processos, liberdade de desenvolvimento da aplicação e escolha na configuração do hardware de máquina. 
Visando a escalabidade da aplicação e as limitações de instanciação da aws, o recomendado é estar iniciando novas instancias conforme for preciso aumentar o processamento da aplicação, pois a aws e outros cloud services não permitem o aumento do processamento das maquinas enquanto estiverem rodando, e este método permite um controle maior sobre o gasto financeiro, na qual podemos aumentar e diminuir as instancias conforme a necessidade de processamento.

* Dev Machine:
    * Dist: Linux Ubuntu 16.10 64bit
    * Kernel: 4.8
    * Hardware: Dell Inspiron
        * Model: 5547
        * CPU: i7-4510U @ 2.00GHz x 4
        * RAM: 8gb 1600mhz

* ERROR LOG:
    *   Virtualbox, erro de inicialização - foi preciso desativar o secure boot na bios, que é ativado ao instalar o Ubuntu a partir da versão 16.04
    *   Vagrant erro de acesso ssh, permissão de chave não permitia trocar para 0600, em pesquisa foi identificado problema de funcionamento do chmod em partições ntfs, movido o repositorio de trabalho para ext4 e funcionou corretamente. (https://github.com/roots/trellis/issues/46)
    *   Ansible: a instalação no Ansible não foi possivel pelo apt-get install pois o pacote para o ubuntu 16.10 não foi implementada, por este motivo foi feita a implementação utilizando o make install, foi necessário a instalação dos pacotes e dependencias do python 2.
    *   Ansible: não estava sendo possível conectar na maquina vagrant usando o Ansible, em testes o acesso via ssh ubuntu@172.17.177.21 -i ~/Projs/chaordic-desafio2/.vagrant/machines/node1/virtualbox/private_key funcionou corretamente, entretanto o ansible não estava permitindo apresentando erros caracteristicos credencias de acesso erradas, a principio pensei que fosse alguma configuração necessária no vagrant mas não encontrei nenhuma solução. Neste caso fiz um pequeno teste com o ansible para executar um comando simples usando as mesmas credenciais do ssh e não consegui: [$ansible all -a "/bin/echo hello" => "172.17.177.21 | FAILED | rc=0 >> MODULE FAILURE"] e pesquisando mais sobre este erro descobri que para o ansible conectar via ssh é necessario que a maquina acessada tenha instalado o python 2.7 sendo que a distribuição do Ubuntu 16.04 não vem com ela instalada por padrão, por isso ao iniciar as maquinas vagrant vai ser necessario inserir o shell de instalação do python 2.7 e algumas dependencias. (source: http://stackoverflow.com/questions/32429259/ansible-fails-with-bin-sh-1-usr-bin-python-not-found)



----------------------------------------------------------------------------------


DESAFIO

O ambiente deve ser todo configurado através de gerenciador de configuração, o que deverá ser entregue é um repositório git contendo os arquivos de configuração que serão aplicados em uma máquina virtual "zerada". Caso necessário descrever como executar o processo de aplicação da configuração na máquina virtual. Ao final da tarefa e execução do processo, deveremos ter um ambiente funcional;

- É recomendado que o repositório git seja entregue com commits parciais, mostrando a evolução de seu código e pensamento. Caso prefira nos informe um url de git público ou então compacte todos os arquivos em um .tar.gz mantendo a pasta .git em conjunto;

- No ambiente deverá estar rodando uma aplicação node.js de exemplo, conforme código abaixo. A versão do node.js deverá ser a última versão LTS disponível em: https://nodejs.org/en/download/. A aplicação node abaixo possui a dependência da biblioteca express. Garanta que seu processo de bootstrap instale essa dependência ( última versão estável disponível em: http://expressjs.com/ ) e rode o processo node em background. De uma forma dinâmica garanta que seja criado uma instância node para cada processador existente na máquina ( a máquina poderá ter de 1 a 32 processadores );
var express = require('express');
var app = express();

app.get('/', function (req, res) {
res.send('Hello World!');
});

app.listen(3000, function () {
console.log('Example app listening on port 3000!');
});

- Construa dentro de sua automação um processo de deploy e rollback seguro e rápido da aplicação node. O deploy e rollback deverá garantir a instalação das dependências node novas (caso sejam adicionadas ou alteradas a versão de algum dependência por exemplo), deverá salvar a versão antiga para possível rollback e reiniciar todos processos node sem afetar a disponibilidade global da aplicação na máquina;

- A aplicação Node deverá ser acessado através de um Servidor Web configurado como Proxy Reverso e que deverá intermediar as conexões HTTP e HTTPS com os clientes e processos node. Um algoritmo de balanceamento deve ser configurado para distribuir entre os N processos node a carga recebida;

- A fim de garantir a disponibilidade do serviço, deverá estar funcional uma monitoração do processo Node e Web para caso de falha, o mesmo deve reiniciar ou subir novamente os serviços em caso de anomalia;

- Desenvolva um pequeno script que rode um teste de carga e demostre qual o Throughput máximo que seu servidor consegue atingir;

- Desenvolva um script que parseie o log de acesso do servidor Web e deverá rodar diariamente e enviar por e-mail um simples relatório, com a frequência das requisições e o respectivo código de resposta (ex:5 /index.html 200);

- Por fim; rode o seu parser de log para os logs gerados pelo teste de carga, garantindo que seu script terá perfomance mesmo em casos de logs com milhares de acessos;

Boa sorte e bons códigos. :)

ps. Qualquer dúvida, o Marcelo está em cópia e poderá te ajudar!

Topics:

- Ambiente configurado pelo gerenciador
- Entregar: 
    * repositório git, com commits parciais, como um log. Gravar no github/bitbucket ou compactar .tar.gz. 
    * criar os arquivos de configuração de deploy e rollback da aplicação
    * descrição de configuração, caso necessário.   
    * Aplicação:
        * Node.js  
            * Last stable
            * Express dependencia mais atualizada
            * Npm install em background
            * Instancia node para cada nucleo do processador
            * Acesso por um servidor web
                * com proxy reverso
                * com intermediação HTTP/HTTPS com os clientes e processos 
        * algoritmo de balanceamento de carga de distribuição dos processos 
        * Automação:
            * deploy
            * rollback seguro e rapido.
                * salvar e restore do rollback
            * garantia de instalação de dependencia adicionais
            * reinicar todos os processos node sem afetar a disponibidade global da aplicação
        * monitoramento dos nodes e web, reiniciar e ou subir novos nodes caso haja erro
        * script de teste de carga e demonstre a carga maxima de throughput do servidor
        * script de parse do log de acesso web, rodando diariamente e enviar um email com um relatório simples com:
            * frequência das requisições e codigo de resposta (ex: 5 /index.html 200)
            * parser de log deve rodar em conjunto com a aplicação para alta carga de processamento. 

Estudo:
    Tecnologias:
        * Docker
            Instalando https://docs.docker.com/engine/installation/linux/ubuntulinux/
            configuração docker group
            http://imasters.com.br/infra/aws/aws-e-as-aplicacoes-dockerizadas/?trace=1519021197&source=category-archive
            Adjust memory and swap accounting
            Configure docker on startup
            Configurando docker para node https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
                Last stable 
                npm install
                npm start
            bash
                sudo docker run -p 49160:3000 -d hara/chaordicapp
                sudo docker ps
                    id: febda074d1a1
                sudo docker logs febda074d1a1
                sudo docker exec -it febda074d1a1 /bin/bash
        * NodeJS
            * Configurando 
        * Puppet 
        * Nginx (proxy)
        * ?Supervisord - gerenciamento de conteiner 
            http://imasters.com.br/infra/supervisord-gerenciando-servicos-em-containers/?trace=1519021197&source=admin
        

